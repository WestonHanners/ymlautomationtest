#import "Specta.h"
#import "ymlCalculator.h"
#define EXP_SHORTHAND
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>

SpecBegin(ymlCalculator)

describe(@"CalculateView", ^{
    
    it(@"should have a button named Calculate", ^{
        UIView *view = [tester waitForViewWithAccessibilityLabel:@"Calculate"];
        
        expect(view).toNot.equal(nil);
    });
    
    it(@"should display 42 in output", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"Calculate"];
        
        [tester tapViewWithAccessibilityLabel:@"Calculate"];
        
        UILabel *result = (UILabel *)[tester waitForViewWithAccessibilityLabel:@"Output"];
        
        expect(result.text).to.equal(@"Value is: 42");
    });
    
    it(@"should display 7 in output when the switch is toggled", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"Switch"];
        
        [tester swipeViewWithAccessibilityLabel:@"Switch" inDirection:KIFSwipeDirectionRight];
        
        UILabel *result = (UILabel *)[tester waitForViewWithAccessibilityLabel:@"Output"];
        
        expect(result.text).to.equal(@"Value is: 7");
    });

});

SpecEnd

