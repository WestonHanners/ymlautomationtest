#import "Specta.h"
#import "ymlCalculator.h"
#define EXP_SHORTHAND
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>

SpecBegin(ymlCalculator)

__block ymlCalculator *sut;

beforeAll(^{
    sut = [ymlCalculator new];
});


describe(@"ymlCalculator Return Values", ^{
    
    it(@"Should not be nil", ^{
        expect(sut).toNot.equal(nil);
    });

        
    it(@"Should Calculate 42", ^{
        expect([sut calculate]).to.equal(@(42));
    });
    
    it(@"Should Not Calculate 41", ^{
        expect([sut calculate]).toNot.equal(@(41));
    });
    
    it(@"Should Calculate 7", ^{
        expect([sut moreCalculations:nil]).to.equal(@(7));
    });
    
});

describe(@"ymlCalculator Delegates", ^{
    
    __block OCMockObject *mockProtocol;

    beforeEach(^{
        mockProtocol = [OCMockObject mockForProtocol:@protocol(ymlCalculatorDelegate)];
        [mockProtocol setExpectationOrderMatters:NO];
        [sut setDelegate:(id)mockProtocol];
    });
    
    it(@"Should deliver a callback on calculate", ^{
        [[mockProtocol expect] didCalculateValue:@(42)];
        
        [sut calculate];
        
        [mockProtocol verifyWithDelay:5];
    });
    
    it(@"Should deliver a callback on moreCalculations", ^{
        [[mockProtocol expect] didCalculateValue:@(7)];
        
        [sut moreCalculations:nil];
        
        [mockProtocol verifyWithDelay:5];
    });
});

SpecEnd
