//
//  ymlFirstViewController.m
//  AutomationTest
//
//  Created by Weston Hanners on 5/5/14.
//  Copyright (c) 2014 Y Media Labs. All rights reserved.
//

#import "ymlFirstViewController.h"
#import "ymlCalculator.h"

@interface ymlFirstViewController () <ymlCalculatorDelegate>

@property (nonatomic) UILabel *output;
@property (nonatomic) UIButton *calculate;
@property (nonatomic) UISwitch *someSwitch;
@property (nonatomic) ymlCalculator *calculator;

@end


@implementation ymlFirstViewController

- (void)viewDidLoad {
    [self.view addSubview:self.output];
    [self.view addSubview:self.calculate];
    [self.view addSubview:self.someSwitch];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(_output, _calculate, _someSwitch);
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[_output]-[_calculate]-[_someSwitch]"
                                                                      options:NSLayoutFormatDirectionLeadingToTrailing
                                                                      metrics:nil
                                                                        views:dict]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_output
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_calculate
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_someSwitch
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];

    
}

- (void)didCalculateValue:(NSNumber *)value {
    [self.output setText:[NSString stringWithFormat:@"Value is: %@", value]];
}

- (UILabel *)output {
    if (!_output) {
        _output = [UILabel new];
        [_output setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_output setText:@"Not Yet Calculated"];
        [_output setAccessibilityLabel:@"Output"];
    }
    return _output;
}

- (UIButton *)calculate {
    if (!_calculate) {
        _calculate = [UIButton new];
        [_calculate setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_calculate setTitle:@"Calculate" forState:UIControlStateNormal];
        [_calculate addTarget:self.calculator action:@selector(calculate) forControlEvents:UIControlEventTouchUpInside];
        [_calculate setAccessibilityLabel:@"Calculate"];
    }
    return _calculate;
}

- (UISwitch *)someSwitch {
    if (!_someSwitch) {
        _someSwitch = [UISwitch new];
        [_someSwitch setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_someSwitch addTarget:self.calculator action:@selector(moreCalculations:) forControlEvents:UIControlEventValueChanged];
        [_someSwitch setAccessibilityLabel:@"Switch"];
    }
    return _someSwitch;
}

- (ymlCalculator *)calculator {
    if (!_calculator) {
        _calculator = [ymlCalculator new];
        [_calculator setDelegate:self];
    }
    return _calculator;
}


@end
