//
//  ymlCalculator.h
//  AutomationTest
//
//  Created by Weston Hanners on 5/5/14.
//  Copyright (c) 2014 Y Media Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ymlCalculatorDelegate <NSObject>

- (void)didCalculateValue:(NSNumber *)value;

@end

@interface ymlCalculator : NSObject

@property (nonatomic, weak) id<ymlCalculatorDelegate> delegate;

- (NSNumber *)calculate;

- (NSNumber *)moreCalculations:(id)sender;

@end
