//
//  ymlCalculator.m
//  AutomationTest
//
//  Created by Weston Hanners on 5/5/14.
//  Copyright (c) 2014 Y Media Labs. All rights reserved.
//

#import "ymlCalculator.h"

@implementation ymlCalculator

- (NSNumber *)calculate {
    NSNumber *returnVal = @(42);
    if (self.delegate) {
        [self.delegate didCalculateValue:returnVal];
    }
    return returnVal;
}

- (NSNumber *)moreCalculations:(id)sender {
    NSNumber *returnVal = @(7);
    if (self.delegate) {
        [self.delegate didCalculateValue:returnVal];
    }
    return returnVal;
}

@end
