//
//  ymlAppDelegate.m
//  AutomationTest
//
//  Created by Weston Hanners on 5/5/14.
//  Copyright (c) 2014 Y Media Labs. All rights reserved.
//

#import "ymlAppDelegate.h"
#import "ymlFirstViewController.h"

@interface ymlAppDelegate ()

@property (nonatomic) UIViewController* rootViewController;

@end

@implementation ymlAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self.window setRootViewController:self.rootViewController];
    [self.window makeKeyAndVisible];
    return YES;
}

- (UIWindow *)window {
    if (!_window) {
        CGRect bounds = [[UIScreen mainScreen] bounds];
        _window = [[UIWindow alloc] initWithFrame:bounds];
        [_window setBackgroundColor:[UIColor redColor]];
    }
    return _window;
}

- (UIViewController *)rootViewController {
    if (!_rootViewController) {
        _rootViewController = [ymlFirstViewController new];
    }
    return _rootViewController;
}

@end
