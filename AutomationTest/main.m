//
//  main.m
//  AutomationTest
//
//  Created by Weston Hanners on 5/5/14.
//  Copyright (c) 2014 Y Media Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ymlAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ymlAppDelegate class]));
    }
}
